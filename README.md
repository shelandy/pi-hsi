pi-hsi （贔屭）

Pi-shi （贔屭） is an electric skateboard design project, aiming at inexpensive (<=$100) way to get a low speed （<25 km/s) boost for feet(pedal) -assistant.  The name of this project comes from the concept of the mythological animal [Pi-shi](https://en.wikipedia.org/wiki/Bixi_%28mythology%29) which was used to carry a very heavy stuff.

([One guy mentioned](http://www.instructables.com/id/Electric-Longboard/step8/Version-20/) he made it $130~150)

Three most expensive parts: motor, ESC and battery

* 40 Amp ESC can find deal under $9 including shipping  in ebay
* Toyota or honda hybrid car used battery is about under $40 on ebay.
* brushless motor: [One article](http://www.electric-skateboard.builders/t/how-to-build-an-electric-skateboard/106) talking about the design   
* *I already have several Arduino / Bluetooth module, Expect to spend ~$10~$20 if you do not have it.

# Design process
I started with converting a roller skate into stake board, then gradually adding stuffs

## handle
ideally, a [foldable handle like this](https://goo.gl/photos/YmtPP6iPUjwDm9D36) for communting is preferred.
the handle is [recycled from a luggage](https://goo.gl/photos/wHy73iTsTmJi8PX56)
